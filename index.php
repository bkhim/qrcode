<?php

 
include('libs/phpqrcode/qrlib.php'); 

function getUsernameFromEmail($email) {
	$find = '@';
	$pos = strpos($email, $find);
	$username = substr($email, 0, $pos);
	return $username;
}

if(isset($_POST['submit']) ) {
	$tempDir = 'temp/'; 
	$email = $_POST['mail'];
	$date =  $_POST['date'];
	$filename = getUsernameFromEmail($email);
    $customer =  $_POST['cname'];
	$amount =  $_POST['amount'];
	$billno  = $_POST['billno'];
	$mode = $_POST['mode'];
    $codeContents = 'Bill No:' . $email . ', DATE:'. $date . ', CUSTOMER:' . $customer . ', AMOUNT:' . $amount . ', Ruf. No:'  . substr(md5(rand()), 0, 12) . ', MODE:'. $mode . ', REF Bank: KCB Mtaani Agent';
	//$codeContents = 'mailto:'.$email.'?subject='.urlencode($subject).'&body='.urlencode($body);
	QRcode::png($codeContents, $tempDir.''.$filename.'.png', QR_ECLEVEL_L, 5);
}
?>
<!DOCTYPE html>
<html lang="en-US">
	<head>
	<title>Blancos QR Reader</title>
	<link rel="stylesheet" href="libs/css/bootstrap.min.css">
	<link rel="stylesheet" href="libs/style.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    </head>
	<body>
		
		<div class="myoutput">
			<div class="input-field">
				<h3>Please Fill All Fields</h3>
				<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" >
					<div class="form-group">
						<label>Receipt No</label>
						<input type="text" class="form-control" name="mail" style="width:20em;" placeholder="Enter Bill Number" value="<?php echo @$email; ?>" required />
					</div>
					<div class="form-group">
						<label>Date</label>
						<input type="date" class="form-control" name="date" style="width:20em;" placeholder="Date" value="<?php echo @$subject; ?>" required pattern="[a-zA-Z .]+" />
					</div>
                    <div class="form-group">
                        <label>Customer Name</label>
                        <input type="text" class="form-control" name="cname" style="width:20em;" placeholder="Enter your Customer Name" value="<?php echo @$subject; ?>" required pattern="[a-zA-Z .]+" />
                    </div>
					<div class="form-group">
						<label>Amount</label>
						<input type="text" class="form-control" name="amount" style="width:20em;" value="<?php echo @$body; ?>" required pattern="[a-zA-Z0-9 .]+" placeholder="Enter Amount"/>
					</div>
                    <div class="form-group">
                        <label>Bill No</label>
                        <input type="text" class="form-control" name="billno" style="width:20em;" value="<?php echo @$body; ?>" required pattern="[a-zA-Z0-9 .]+" placeholder="Enter Amount"/>
                    </div>
                    <div class="form-group">
                        <label>Payment Mode</label>
                        <select type="text" class="form-control" name="mode" style="width:20em;" required pattern="[a-zA-Z0-9 .]+" placeholder="Enter Amount">
                            <option value="Direct Banking">Direct Banking</option>
                            <option value="Mobile Payment">Mobile Payment</option>
                        </select>
                    </div>

					<div class="form-group">
						<input type="submit" name="submit" class="btn btn-primary submitBtn" style="width:20em; margin:0;" />
					</div>
				</form>
			</div>
			<?php
			if(!isset($filename)){
				$filename = "author";
			}
			?>
			<div class="qr-field">
				<h3>QR Code Result: </h3>
				<center>
					<div class="qrframe" style="border:2px solid black; width:210px; height:210px;">
							<?php echo '<img src="temp/'. @$filename.'.png" style="width:200px; height:200px;"><br>'; ?>
					</div>
					<a class="btn btn-primary submitBtn" style="width:210px; margin:5px 0;" href="download.php?file=<?php echo $filename; ?>.png ">Download QR Code</a>
				</center>
			</div>
			
		</div>
        <style>
            .footer{
                text-align: center;
                font-size: 130%;
            }
            .footer a{
                padding: 0 5px 0 5px;
            }
        </style>
    <div class="footer">
        <p>
            Created By Blancos Khim<br/>
            <i class="fa fa-facebook-official fa-lg"></i> <a target="_blank" href="https://www.facebook.com/blancoskhym">Find Me on Facebook</a> |
            <i class="fa fa-twitter fa-lg"></i><a target="_blank" href="https://twitter.com/blancoskhim">Find Me on Twitter</a> |
            <i class="fa fa-linkedin fa-lg"></i><a target="_blank" href="https://www.linkedin.com/in/brian-kimathi/">Find Me On LinkedIn</a>
        </p>
    </div>
	</body>
</html>